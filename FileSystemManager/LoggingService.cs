﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace FileSystemManager
{
    class LoggingService
    {
        public const string logPath = @"resources\log.txt";

        // Method for logging the search word, occurrences of the word and execution time.
        public static void logSearch(string searchWord, int wordCount, int executionTime)
        {
            string path = Path.Combine(Environment.CurrentDirectory, logPath);

            string currentDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm");

            string logMessage = $"{currentDate}: The word \"{searchWord}\" was found {wordCount} times." +
                $" The function took {executionTime}ms to execute.";

            try
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(logMessage);
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex);
            }

        }
    }
}
