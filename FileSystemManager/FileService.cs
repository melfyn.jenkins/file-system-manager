﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace FileSystemManager
{
    class FileService
    {

        public const string draculaPath = @"resources\Dracula.txt";
        public const string resourcesPath = @"resources";

        // Return the name of the file (Without the extension)
        public static string getFileName()
        {
            string path = Path.Combine(Environment.CurrentDirectory, draculaPath);
            string fileName = "";

            try
            {
                fileName = Path.GetFileNameWithoutExtension(path);
            } 
            catch(FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            return fileName;
        }

        // Reads the file line by line and counts the number of lines read
        public static int countFileLines()
        {
            int lineCount = 0;

            string path = Path.Combine(Environment.CurrentDirectory, draculaPath);

            try
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string s;
                    while ((s = sr.ReadLine()) != null)
                    {
                        lineCount++;
                    }
                }
            } 
            catch(FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
   
            return lineCount;

        }

        // Returns the file size of Dracula.txt
        public static long getFileSize()
        {
            long fileSize = 0;
            string path = Path.Combine(Environment.CurrentDirectory, draculaPath);

            try
            {
                fileSize = new System.IO.FileInfo(path).Length;
                // converting the result from byte to kb
                fileSize = fileSize / 1024;
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return fileSize;
        }

        // Lists all files from resources directory
        public static void listFiles()
        {
            string path = Path.Combine(Environment.CurrentDirectory, resourcesPath);

            try
            {
                string[] files = Directory.GetFiles(path);
                foreach (string file in files)
                {
                    Console.WriteLine(Path.GetFileName(file));
                }

            }
            catch(DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(UnauthorizedAccessException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Get files with specific extensions based on user input
        public static void getFilesByExtension(string extention)
        {
            string path = Path.Combine(Environment.CurrentDirectory, resourcesPath);

            try
            {
                string[] files = Directory.GetFiles(path, "*."+extention);
                foreach (string file in files)
                {
                    Console.WriteLine(Path.GetFileName(file));
                }

            }
            catch (DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // Get all unique file extensions from resources folder
        public static List<string> getFileExtensions()
        {
            string path = Path.Combine(Environment.CurrentDirectory, resourcesPath);

            List<string> extensions = new List<string>();

            try
            {
                string[] files = Directory.GetFiles(path);
                
                foreach (string file in files)
                {
                    extensions.Add(Path.GetExtension(file));
                }
            }
            catch(DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Storing unique extensions in List
            List<string> uniqueExtensions = extensions.Distinct().ToList();

            // Removing the dot from every unique extension
            for(int i = 0; i < uniqueExtensions.Count; i++)
            {
                uniqueExtensions[i] = uniqueExtensions[i].Replace(".", "");
            }

            return uniqueExtensions;

        }

        // Searches for the word in Dracula.txt and returns the number of occurrences of that word.
        public static int searchFile(string searchWord)
        {
            string path = Path.Combine(Environment.CurrentDirectory, draculaPath);

            int results = 0;
            try
            {
                // Reads file to string and makes all the text lower case
                string readText = File.ReadAllText(path).ToLower();
                // Searches for input word and counts occurrences
                results = Regex.Matches(readText, $@"\b{searchWord}\b").Count;

            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            return results;

        }
        // Check if user input is an int. Returns the number when it's validated
        public static int validateNumberInput(string input)
        {
            int number = 0;

            while (!int.TryParse(input, out number))
            {
                Console.WriteLine("Invalid input. Try again: ");
                input = Console.ReadLine();
            }

            return number;
        }

    }
}
