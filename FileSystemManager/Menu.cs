﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FileSystemManager
{
    public class Menu
    {
        // Main menu with all available options
        public static void mainMenu()
        {
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("File System Manager");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("Choose what you want to do by typing a number: ");
            Console.WriteLine("1. List information about Dracula.txt");
            Console.WriteLine("2. Search in Dracula.txt");
            Console.WriteLine("3. List files in resources directory");
            Console.WriteLine("4. Search for files based on extension in resources directory");
            Console.WriteLine("(Type 0 to exit to program)");
            Console.WriteLine("-----------------------------------");
            string menuOptionInput = Console.ReadLine();
            int menuOption = FileService.validateNumberInput(menuOptionInput);
            int firstOption = 1;
            int secondOption = 2;
            int thirdOption = 3;
            int fourthOption = 4;
         
            if (menuOption >= firstOption && menuOption <= fourthOption)
            {
                if (menuOption == firstOption)
                {
                    Menu.draculaInfoOption();
                }
                else if (menuOption == secondOption)
                {
                    Menu.searchOption();
                }
                else if (menuOption == thirdOption)
                {
                    Menu.listAllFilesOption();
                }
                else if (menuOption == fourthOption)
                {
                    Menu.listFilesByExtensionOption();
                }
            }
            else
            {
                Console.WriteLine("Bye Bye!");
            }
        }
        // Lists file information about dracula.txt
        public static void draculaInfoOption()
        {
            string filename = FileService.getFileName();
            long fileSize = FileService.getFileSize();
            int lineCount = FileService.countFileLines();

            Console.WriteLine("-----------------------------------");
            Console.WriteLine("Information about Dracula.txt");
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("Name: " + filename);
            Console.WriteLine("File size: " + fileSize + "KB");
            Console.WriteLine("Number of lines: " + lineCount);
            Console.WriteLine("-----------------------------------");

            mainMenu();
        }
        // Lists all files in resources directory
        public static void listAllFilesOption()
        {
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("All files in resources directory");
            Console.WriteLine("-----------------------------------");
            FileService.listFiles();
            Console.WriteLine("-----------------------------------");

            mainMenu();
        }
        // Lists files with specific extension from resources folder
        public static void listFilesByExtensionOption()
        {
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("File types in resources directory");
            Console.WriteLine("-----------------------------------");

            List<string> extension = FileService.getFileExtensions();

            for (int i = 0; i < extension.Count; i++)
            {
                Console.WriteLine(i + 1 + ". " + extension[i]);
            }

            int exitNumber = extension.Count + 1;
            Console.WriteLine("-----------------------------------");
            Console.WriteLine("Enter the number of the extension you want to list: ");
            Console.WriteLine($"(Type {exitNumber} to exit main menu)");
            Console.WriteLine("-----------------------------------");
            string extensionOptionInput = Console.ReadLine();
            int extensionOption = FileService.validateNumberInput(extensionOptionInput) - 1;

            if (extensionOption != exitNumber - 1)
            {
                while (extensionOption != exitNumber - 1)
                {
                    if (extensionOption <= extension.Count && extensionOption >= 0)
                    {
                        FileService.getFilesByExtension(extension[extensionOption]);
                        Console.WriteLine("-----------------------------------");

                        extensionOptionInput = Console.ReadLine();
                        extensionOption = FileService.validateNumberInput(extensionOptionInput) - 1;
                    }
                    else
                    {
                        Console.WriteLine("Invalid input. Try again: ");
                        extensionOptionInput = Console.ReadLine();
                        extensionOption = FileService.validateNumberInput(extensionOptionInput) - 1;
                    }

                }
            }

            mainMenu();

        }

        // Dracula search option. User can search for words in the Dracula.txt
        public static void searchOption()
        {
            string searchWord = "";
            while (searchWord != "0")
            {
                Console.WriteLine("-----------------------------------");
                Console.WriteLine("Type a word you want to search for:");
                Console.WriteLine("(Type 0 to exit to main menu)");
                Console.WriteLine("-----------------------------------");
                searchWord = Console.ReadLine().ToLower();

                // Time before and after execution is stored to calculate execution time in ms.
                DateTime beforeExecution = DateTime.Now;
                int wordCount = FileService.searchFile(searchWord);
                DateTime afterExecution = DateTime.Now;
                int executionTime = afterExecution.Millisecond - beforeExecution.Millisecond;

                if (searchWord != "0")
                {
                    string resultMessage = $"The word \"{searchWord}\" was found {wordCount} times." +
                  $" The function took {executionTime}ms to execute.";
                    Console.WriteLine(resultMessage);
                    // Logging the search to log.txt
                    LoggingService.logSearch(searchWord, wordCount, executionTime);

                }
            }

            mainMenu();
        }

    }
}
