# File System Manager
File system manager is a console application made with c#. The system includes different types of files which the user can interact with. This includes reading, searching for words in text-file and listing files in a directory.
## Features
### Dracula.txt
- Lists information about the file: Filename, size, line count
- User can search for specific words in the document
    - Search result tells the user if the word exists and how many times the word is found in the file.
### Resource directory
- Listing all file names in directory
- List files based on extensions
### Logging
- Logs all searched words in log.txt with the word, date and time it took to execute the function
